from fuzzywuzzy import fuzz
import concurrent.futures
import numpy as np
import pandas as pd


mhe = pd.read_csv('mhe.csv')
cengage = pd.read_csv('cengage.csv')

mhe_cols = ['NAME', 'PID__C', 'LOW_GRADE__C', 'SHIPPINGCITY', 'HIGH_GRADE__C', 'SHIPPINGSTATE', 'SHIPPINGSTREET',
            'ACCOUNT_CLASS__C']
cengage_cols = ['Institution Name', 'PID', 'LOGrade', 'Mailing City', 'HIGrade', 'Mailing State', 'Mailing Address',
                'InstitutionType']

mhe = mhe[mhe_cols]
cengage = cengage[cengage_cols]
mhe.columns = cengage.columns


def get_max_weight(val):
    temp_weigh_list = []
    row_cen = val
    cengage_row = cengage.iloc[row_cen].values
    for row_mhe in range(mhe.shape[0]):
        fuzz_values = [fuzz.ratio(x, y) for x, y in zip(cengage_row, mhe.iloc[row_mhe].values)]
        weights = [0.175, 0.175, 0.075, 0.175, 0.075, 0.075, 0.175, 0.075]
        weighted_fuzz = [i * j for i, j in zip(fuzz_values, weights)]
        temp_weigh_list.append(weighted_fuzz)

    temp_weigh_scores = pd.DataFrame(data=temp_weigh_list)
    temp_weigh_scores["Sum"] = temp_weigh_scores.sum(axis=1)

    max_idx = temp_weigh_scores["Sum"].idxmax()
    max_values = temp_weigh_scores.iloc[max_idx].values

    return np.append([row_cen, max_idx], max_values)


nums = range(cengage.shape[0])


def main():
    with concurrent.futures.ProcessPoolExecutor() as executor:
        final_list = [val for val in executor.map(get_max_weight, nums)]
    final_list = pd.DataFrame(final_list,
                              columns=['cen_idx', 'mhe_idx', 's1', 's2', 's3', 's4', 's5', 's6', 's7', 's8', 'sum'])
    final_list.to_csv('scores.csv')


if __name__ == '__main__':
    main()
